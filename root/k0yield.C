double efficiency(TH1D* h1,TH1D* h2,TString name,bool fraction=false ) {
  // H1 pass, h2 fail
    TF1 * fn1 = new TF1(name+"k0fit1","[0]*exp(-0.5*([1]-x)*([1]-x)/([2]*[2]))+[3]+[4]*(x-[1])",450,550);
    TF1 * fn2 = new TF1(name+"k0fit2","[0]*exp(-0.5*([1]-x)*([1]-x)/([2]*[2]))+[3]+[4]*(x-[1])",450,550);
    fn1->SetParameter(0,1400);
    fn1->SetParameter(1,497.);
    fn1->SetParameter(2,7.);
    fn1->SetParameter(3,140);
    fn1->SetParameter(4,-0.6);

    fn2->SetParameter(0,1400);
    fn2->SetParLimits(0,0,1E6);
    fn2->SetParameter(1,497.);
    fn2->SetParLimits(1,496,498);
    fn2->SetParameter(2,7.);
    fn2->SetParLimits(2,4,15);
    fn2->SetParameter(3,140);
    fn2->SetParameter(4,-0.6);

    TCanvas * c = new TCanvas(name+"_c",name+"_c",800,600);

    h1->Fit(fn1,"lR");
    TF1 * passfn = h1->GetFunction(name+"k0fit1");
    double ratepass = passfn->GetParameter(0)*passfn->GetParameter(2);


    h2->Fit(fn2,"lR");
    TF1 * failfn = h2->GetFunction(name+"k0fit2");
    failfn->SetLineColor(kBlue);
    double ratefail = failfn->GetParameter(0)*failfn->GetParameter(2);

    h2->GetXaxis()->SetRangeUser(400,600);
    h2->SetMarkerStyle(20);
    h2->SetMarkerColor(kBlue);
    double max = h1->GetMaximum();
    if (h2->GetMaximum() > max) max = h2->GetMaximum();
    h2->SetMaximum(1.05*max);
    
    h1->SetLineColor(kRed);
    h1->SetMarkerColor(kRed);
    h1->SetMarkerStyle(21);
    h2->SetMinimum(0.);
    h2->Draw("pe ");
    h1->Draw("pe same");

    double frac;
    if (fraction) frac=ratepass/(ratefail);
    else frac=ratepass/(ratefail+ratepass);

    char tmpchar[100];

     sprintf(tmpchar," Eff=%4.2f%%",frac*100);
     TString title=name+tmpchar;

     TLegend * leg = new TLegend(0.6,0.7,0.9,0.9);
    leg->SetHeader(title);
    leg->AddEntry(h1,"pass","p");
    if (fraction) leg->AddEntry(h2,"Total","p");
    else leg->AddEntry(h2,"Fail","p");
    leg->Draw();
    std::cout<<"Fraction passing is "<<frac<<std::endl;
     c->SaveAs(name+".png");

    return frac;
}

void doRatio(TH1D* h1,TH1D* h2,TString name) {
  // 
  TCanvas * c = new TCanvas(name+"_ratio",name+"_ratio",800,800);
  c->Divide(1,2);
  c->cd(1);
  h1->Draw();
  h2->SetLineColor(kRed);
  h2->Draw("same");

  TPad * pad = (TPad*)c->cd(2);
  pad->SetGridx();
  pad->SetGridy();
  TEfficiency *  Eff = new TEfficiency(*h2,*h1);

   Eff->Draw("AP");
   c->SaveAs("eff_"+name+".png");

}

void fitpi0(TH1D* h1,TString name) {
  // 
  TCanvas * c = new TCanvas(name+"_fitpi0",name+"_fitpi0",800,600);
  c->SetGridx();
  c->SetGridy();

  TF1 * fn = new TF1(name+"pi0fit","[0]*exp(-0.5*([1]-x)*([1]-x)/([2]*[2]))+[3]+[4]*(x-[1])",80,250);
  fn->SetParameter(0,8);
  fn->SetParameter(1,137.);
  fn->SetParameter(2,9.);
  fn->SetParameter(3,4);
  fn->SetParameter(4,-0.01);

  h1->Fit(fn,"lR");
  h1->GetXaxis()->SetRangeUser(0.,300.);
  h1->SetMarkerStyle(20);
  h1->Draw("pe");
  c->SaveAs("fitpi0_"+name+".png");

}

void centre(TH2D* xy) {
  int nx = 50;
  double x1 = -4.;
  double x2 = 6.;
  int ny = 50;
  double y1 = -4.;
  double y2 = 6.;
  TH2D * maxHeight = new TH2D("maxHeight","maxHeight",nx,x1,x2,ny,y1,y2);
  int nbx = xy->GetXaxis()->GetNbins();
  int xmin = xy->GetXaxis()->GetXmin();
  int xmax = xy->GetXaxis()->GetXmax();
  int nby = xy->GetYaxis()->GetNbins();
  int ymin = xy->GetYaxis()->GetXmin();
  int ymax = xy->GetYaxis()->GetXmax();

  TH1D * radial = new TH1D("radial","radial",35,0.,350.);
  for (int ix=0;ix<nx;++ix) {
    double xc = x1 + (x2-x1)/nx*(ix+0.5);
    for (int iy=0;iy<ny;++iy) {
      double yc = y1 + (y2-y1)/ny*(iy+0.5);
      radial->Reset();
      for (int i=0;i<nbx;++i) {
	double x = xmin + (xmax-xmin)*(i+0.5)/nbx;
	for (int j=0;j<nby;++j) {
	  double y = ymin + (ymax-ymin)*(j+0.5)/nby;
	  double rad = sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc));
	  double nconv = xy->GetBinContent(i,j);
	  radial->Fill(rad,nconv);
	}
      }
      if (ix==0 && iy==0) radial->DrawClone();
      double peak = radial->GetMaximum();
      maxHeight->Fill(xc,yc,peak);
    }
  }

  TCanvas * can = new TCanvas("maxHeight","maxHeight",800,600);
  maxHeight->Draw("colz");
}

void k0yield() {
  gStyle->SetOptStat(0);
  TFile *_file0 = TFile::Open("../run/submitDir/hist-user.tamartin.data15_13TeV.00267385.physics_MinBias.merge.RAW_der1574869751_toAOD_v2_EXT0.root");
  TH1D * h_k0_k0_mass_failchi = (TH1D*)gROOT->FindObject("h_k0_k0_mass_failchi");
  TH1D * h_k0_k0_mass_passchi = (TH1D*)gROOT->FindObject("h_k0_k0_mass_passchi");
  TH1D * h_k0_k0_mass_pix = (TH1D*)gROOT->FindObject("h_k0_k0_mass_pix");
  TH1D * h_k0_k0_mass_nopix = (TH1D*)gROOT->FindObject("h_k0_k0_mass_nopix");
  TH1D * h_k0_k0_mass = (TH1D*)gROOT->FindObject("h_k0_k0_mass");
  TH1D * h_k0_k0_mass_faildphi = (TH1D*)gROOT->FindObject("h_k0_k0_mass_faildphi");

  TH1D * h_k0_k0_mass_passRxyError = (TH1D*)gROOT->FindObject("h_k0_k0_mass_passRxyError");
  TH1D * h_k0_k0_mass_failRxyError = (TH1D*)gROOT->FindObject("h_k0_k0_mass_failRxyError");

  TH1D * h_x17_mk0 = (TH1D*)gROOT->FindObject("h_x17_mk0");
  TH1D * h_x17_mk0_noDphi = (TH1D*)gROOT->FindObject("h_x17_mk0_noDphi");
  TH1D * h_x17_mk0_noSCT = (TH1D*)gROOT->FindObject("h_x17_mk0_noSCT");
  TH1D * h_x17_mk0_noPix = (TH1D*)gROOT->FindObject("h_x17_mk0_noPix");
  TH1D * h_x17_mk0_noZ = (TH1D*)gROOT->FindObject("h_x17_mk0_noZ");

  TH1D * h_x17_m = (TH1D*)gROOT->FindObject("h_x17_m");
  TH1D * h_x17_m_resVeto = (TH1D*)gROOT->FindObject("h_x17_m_resVeto");
  TH1D * h_x17_m_rdVeto = (TH1D*)gROOT->FindObject("h_x17_m_rdVeto");
  TH1D * h_x17_m_noDphi = (TH1D*)gROOT->FindObject("h_x17_m_noDphi");
  TH1D * h_x17_m_noZ = (TH1D*)gROOT->FindObject("h_x17_m_noZ");
  TH1D * h_x17_m_noPix = (TH1D*)gROOT->FindObject("h_x17_m_noPix");
  TH1D * h_x17_m_noSCT = (TH1D*)gROOT->FindObject("h_x17_m_noSCT");
  TH1D * h_x17_m_noR = (TH1D*)gROOT->FindObject("h_x17_m_noR");
  TH1D * h_x17_m_noChi2 = (TH1D*)gROOT->FindObject("h_x17_m_noChi2");
  TH1D * h_x17_m_noRxyError = (TH1D*)gROOT->FindObject("h_x17_m_noRxyError");
  TH1D * h_x17_r_v_m_resVeto = (TH1D*)gROOT->FindObject("h_x17_r_v_m_resVeto");
  h_x17_r_v_m_resVeto->GetXaxis()->SetTitle("Mass, GeV");
  h_x17_r_v_m_resVeto->GetYaxis()->SetTitle("Radius, mm");
  TH2D * h_x17_y_v_x = (TH2D*)gROOT->FindObject("h_x17_y_v_x");

  TH1D * h_pi0_all = (TH1D*)gROOT->FindObject("h_pi0_all");
  TH1D * h_pi0_passPtRat = (TH1D*)gROOT->FindObject("h_pi0_passPtRat");
  TH1D * h_pi0_rMin = (TH1D*)gROOT->FindObject("h_pi0_rMin");

  
  efficiency(h_k0_k0_mass,h_k0_k0_mass_faildphi,"passdphi");
  efficiency(h_k0_k0_mass_passchi,h_k0_k0_mass_failchi,"passchi");
  efficiency(h_k0_k0_mass_nopix,h_k0_k0_mass_pix,"passpix");
  efficiency(h_k0_k0_mass_passRxyError,h_k0_k0_mass_failRxyError,"passRxyError");
  
  efficiency(h_x17_mk0,h_x17_mk0_noDphi,"passdphi_x17",true);
  efficiency(h_x17_mk0,h_x17_mk0_noSCT,"passSCT_x17",true);
  efficiency(h_x17_mk0,h_x17_mk0_noPix,"passPix_x17",true);
  efficiency(h_x17_mk0,h_x17_mk0_noZ,"passZ_x17",true);

  

  doRatio(  h_x17_m,h_x17_m_resVeto,"resVeto");
  doRatio(  h_x17_m_noDphi,h_x17_m_resVeto,"Dphi");
  doRatio(  h_x17_m_noZ,h_x17_m_resVeto,"Z");
  doRatio(  h_x17_m_noR,h_x17_m_resVeto,"R");
  doRatio(  h_x17_m_noPix,h_x17_m_resVeto,"Pix");
  doRatio(  h_x17_m_noSCT,h_x17_m_resVeto,"SCT");
  doRatio(  h_x17_m_noChi2,h_x17_m_resVeto,"Chi2");
  doRatio(  h_x17_m_noRxyError,h_x17_m_resVeto,"RxyError");

  TCanvas * c = new TCanvas("r_v_m","r_v_m",800,600);
  c->SetGridx();
  c->SetGridy();
  c->SetLogz();
  h_x17_r_v_m_resVeto->Draw("colz");
  c->SaveAs("r_v_m.png");

  TCanvas * cm = new TCanvas("m","m",800,600);
  cm->SetGridx();
  cm->SetGridy();
  cm->SetLogz();
  h_x17_m_resVeto->GetXaxis()->SetRangeUser(0.,100.);
  h_x17_m_resVeto->SetMarkerStyle(20);
  h_x17_m_resVeto->SetMarkerColor(1);
  h_x17_m_resVeto->SetLineColor(1);
  h_x17_m_resVeto->GetXaxis()->SetTitle("ee Mass [MeV]");
  h_x17_m_resVeto->GetYaxis()->SetTitle("Events / 2.5 MeV");
  h_x17_m_resVeto->Draw("pe");
  h_x17_m_rdVeto->SetMarkerStyle(21);
  h_x17_m_rdVeto->SetMarkerColor(2);
  h_x17_m_rdVeto->Draw("same pe");
  c->SaveAs("r_v_m.png");

  TCanvas * cxy = new TCanvas("xy","xy",800,800);
  cxy->SetGridx();
  cxy->SetGridy();
  cxy->SetLogz();
  h_x17_y_v_x->SetMarkerStyle(20);
  h_x17_y_v_x->SetMarkerSize(0.5);
  h_x17_y_v_x->Draw("p");

  TEllipse * circ1 = new TEllipse (1.0,0.5,150.,150.);
  TEllipse * circ2 = new TEllipse (1.0,0.5,200.,200.);
  circ1->SetFillStyle(0);
  circ2->SetFillStyle(0);
  circ1->SetLineStyle(2);
  circ2->SetLineStyle(2);
  circ1->SetLineColor(2);
  circ2->SetLineColor(2);
  circ1->Draw();
  circ2->Draw();

  
   cxy->SaveAs("xy.png");

   fitpi0(h_pi0_all,"pi0_all");
   fitpi0(h_pi0_passPtRat,"pi0_passPtRat");
   fitpi0(h_pi0_rMin,"pi0_rMin");
  //  centre(h_x17_y_v_x);
}
