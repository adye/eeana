#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse, sys, os, re, pwd, subprocess, random, string, time

def main():
  parser = optparse.OptionParser()
  parser.add_option ('-s', '--submission-dir', dest='submission_dir', default='submitDir', help='Submission directory for EventLoop')
  parser.add_option ('-e', '--evtMax',         type='int',                                 help='Maximum number of events to run over')
  parser.add_option ('-n', '--filesPerWorker', type='int',                                 help='allocate this many files to each job')
  parser.add_option ('-o', '--outDS',                                                      help='Output dataset name')
  parser.add_option ('-l', '--local',          action='store_true',                        help='simulates submitting to a batch system on a single machine')
  parser.add_option ('-c', '--condor',         action='store_true',                        help='Submit to Condor batch system')
  parser.add_option ('-g', '--grid',           action='store_true',                        help='Submit to the Grid')
  parser.add_option ('-w', '--wait',           action='store_true',                        help='Wait for and retrieve previously-submitted jobs\' output')
  parser.add_option ('-r', '--retrieve',       action='store_true',                        help='Retrieve previously-submitted jobs\' output if available')
  ( options, args ) = parser.parse_args()
  if options.filesPerWorker and not (options.local or options.condor or options.grid): options.condor = True

  if len(args) == 0:
# Default inputs:
#   args += [os.getenv('ALRB_TutorialData') + '/r9315/AOD.11182705._000001.pool.root.1']
#   args += ['/data/atlas/phsmai/ee/user.tamartin.data15_13TeV.00267385.physics_MinBias.merge.RAW_der1574869751_toAOD_v2_EXT0/user.tamartin.19904505.EXT0._0000*.AOD.pool.root']
#   args += ['/pnfs/pp.rl.ac.uk/data/atlas/atlasdatadisk/rucio/data18_13TeV/5f/5a/DAOD_RPVLL.21910841._000011.pool.root.1']
    args += ['user.adye:user.tamartin.data15_13TeV.00267385.physics_MinBias.merge.RAW_der1574869751_toAOD_v2_EXT0']
#   args += ['user.adye:user.tamartin.mc16_13TeV.361224.Epos_minbias_inelastic.simul.HITS.e3908_e5984_s3245_tid18896522_00_toAOD_v1_EXT0']
#   args += ['data15_13TeV:data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_RPVLL.pro25_v04']
#   args += ['data16_13TeV:data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_RPVLL.pro25_v04']
#   args += ['data17_13TeV:data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_RPVLL.pro25_v04']
#   args += ['data18_13TeV:data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_RPVLL.pro25_v04']

  jobtype = "eljob"
  prog = os.path.basename( sys.argv[0] )
  ##timestamp = datetime.datetime.now().strftime("%Y-%m-%d-%H%S")
  ## shorter random string instead
  timestamp = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(4))

  # Set up (Py)ROOT.
  import ROOT
  ROOT.xAOD.Init().ignore()

  if options.wait:     return results( options.submission_dir, 20 )
  if options.retrieve: return results( options.submission_dir )

  # Set up the sample handler object. See comments from the C++ macro
  # for the details about these lines.
  sh = ROOT.SH.SampleHandler()
  sh.setMetaString( 'nc_tree', 'CollectionTree' )

  # Specify ROOT file, local ROOT file wildcard, file list, or Rucio dataset as command argument
  jobname = None
  for arg in args:
    argname = os.path.basename(arg)
    if not jobname: jobname = re.sub( r"^[\w.]+:", "", argname)+"."+jobtype+"-"+timestamp
    if re.search (r"^\w+://", arg):
      sample = ROOT.SH.SampleLocal (argname)
      sample.add(arg)
      sh.add (sample)
    elif re.search (r"\.root(\.\d+)?$", arg):
      ROOT.SH.ScanDir().filePattern( argname ).scan( sh, os.path.dirname(arg) )
    elif os.path.isfile(arg):
      ROOT.SH.readFileList(sh, argname, arg)
    else:
      ROOT.SH.scanRucio(sh, arg)
  sh.printContent()
  if not jobname: jobname = jobtype+"-"+timestamp
  # Can't query sh[0].name() because that seems to mess up subsequent Grid submission.

  print "Submit job name '"+jobname+"'"

  # Create an EventLoop job.
  job = ROOT.EL.Job()
  job.sampleHandler( sh )
  if options.evtMax:
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, options.evtMax )
  job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')
  job.options().setDouble( ROOT.EL.Job.optCacheSize, 10*1024*1024 )
  if options.filesPerWorker:
    job.options().setDouble (ROOT.EL.Job.optFilesPerWorker, options.filesPerWorker)

  # Create the algorithm's configuration.
  from AnaAlgorithm.DualUseConfig import createAlgorithm
  alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

  job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )

  # later on we'll add some configuration options for our algorithm that go here

  # Add our algorithm to the job
  job.algsAdd( alg )

  if options.local:

    driver = ROOT.EL.LocalDriver()

  elif options.condor:

    os.environ.setdefault ('WorkDir_DIR', os.environ['UserAnalysis_DIR'])   # why is this needed?
    driver = ROOT.EL.CondorDriver()
    sendProxy(driver)

  elif options.grid:

    driver = ROOT.EL.PrunDriver()
    if options.outDS:
      driver.options().setString( "nc_outputSampleName", options.outDS )
    else:
      user = os.environ.get( "RUCIO_ACCOUNT", pwd.getpwuid(os.getuid())[0] )
      driver.options().setString( "nc_outputSampleName", "user."+user+"."+jobname )
    driver.options().setDouble( ROOT.EL.Job.optGridMergeOutput, 1 )
    if options.filesPerWorker:
      driver.options().setDouble( ROOT.EL.Job.optGridNFilesPerJob, options.filesPerWorker )

  else:

    driver = ROOT.EL.DirectDriver()

  # Run the job using the selected batch system driver.
  if options.condor or options.grid:
    if options.condor: os.umask( 077 )    # don't expose user's Grid proxy
    try:
      subdir = driver.submitOnly( job, options.submission_dir )
##    if options.condor: os.chmod( options.submission_dir+"/submit/run", 0700 )  # if we don't want to use umask, just protect the secret file (with a small delay)
    except Exception as e:
      s = parse_cpp_exception(e)
      if s: print(s)
      else: raise
      return 127

    if subdir is None: subdir = options.submission_dir  # didn't return subdir in 21.2.165 and before
    print "Submitted jobs in", subdir
    if options.grid: time.sleep(2.0)
## This doesn't seem to work after driver.submitOnly, so do retrieve with subprocess.
##  ok = ROOT.EL.Driver.retrieve ( subdir )
    ret = subprocess.call ([__file__, '-r', '-s', subdir])
    if ret == 1:
      print "Use",prog,"-w or -r to retrieve results."
  else:
    try:
      subdir = driver.submit( job, options.submission_dir )
      if subdir is None: subdir = options.submission_dir
      print "Jobs in", subdir, "done."
      return 0
    except Exception as e:
      s = parse_cpp_exception(e)
      if s: print(s)
      else: raise
      return 127


#=======================================================================
# EventLoop submission utility routines
#=======================================================================

def sendProxy (driver):
# Setting x509userproxy might be enough for some Condor systems, but doesn't work for PPD Linux, so instead have to send it with the job script.
#    job.options().setString (ROOT.EL.Job.optCondorConf, "x509userproxy = /tmp/x509up_u"+str(os.getuid()))
  proxy= os.environ.get ("X509_USER_PROXY", "/tmp/x509up_u"+str(os.getuid()))
  jobProxy= '.x509up/'+os.path.basename(proxy)
  try:
    with open(proxy) as fproxy:
      proxyData = fproxy.read().rstrip('\n')
      driver.shellInit = '''(
  umask 077
  mkdir "'''+os.path.dirname(jobProxy)+'''"
  cat <<END_OF_USER_PROXY > "'''+jobProxy+'''"
'''+proxyData+'''
END_OF_USER_PROXY
)
export X509_USER_PROXY="`pwd`/'''+jobProxy+'''"'''
  except EnvironmentError:
    pass


def results (subdir, wait=0):
  import ROOT
  # disable spurious 'deprecated' warnings
  ROOT.gInterpreter.ProcessLine('#pragma clang diagnostic ignored "-Wdeprecated-declarations"')
  try:
    if wait:
      ok = ROOT.EL.Driver.wait ( subdir, wait )
    else:
      ok = ROOT.EL.Driver.retrieve ( subdir )
    if not wait:
      if ok:
        print "Jobs in", subdir, "are now complete."
      else:
        print "Jobs in", subdir, "are still running."
    return ( 0 if ok else 1 )
  except Exception as e:
    s = parse_cpp_exception(e)
    if s: print(s)
    else: raise
    return 127


# Fix C++ exception message as discussed here:
# https://its.cern.ch/jira/browse/ATLASG-1584
# The overload was removed in 21.2.166, so the fixup is no longer necessary after that.
def parse_cpp_exception (e):
  import re
  try:    from cppyy.gbl import std
  except: pass
  s = [ss.strip() for ss in str(e).split("\n")]
  if type(e) is TypeError and len(s)>=5 and len(s)%2==1:
    if not re.match(r"none of the \d+ overloaded methods succeeded\. Full details:$", s[0]): return
    s = [[s[i],s[i+1]] for i in range(1,len(s)-1,2) if not re.match (r"(TypeError: )?takes at (least|most) \d+ arguments \(\d+ given\)$", s[i+1])]
    if len(s) != 1: return
    s = s[0]
  elif not (len(s)==2 and (type(e) is Exception or ('std' in vars() and isinstance(e,std.exception)))):
    return
  sub, msg = s
  sub, ok = re.subn(r"\s+=>$", "", sub)
  if not ok: return
  m = re.search(r"(.*?)\s+\(\C\+\+ exception of type (\S+)\)$", msg)
  if m:
    msg, typ = m.group(1), m.group(2)
  else:
    m = re.match(r"(\w+): (.*)$", msg)
    if not m: return
    typ, msg = m.group(1), m.group(2)
    try:
      if not isinstance(getattr(std,typ)(),std.exception): return
    except:
      return
  if type(e) is TypeError: typ = typ+" (mangled as "+str(type(e).__name__)+")"
  return "C++ exception of type "+typ+" in "+sub+": "+msg


sys.exit(main())
