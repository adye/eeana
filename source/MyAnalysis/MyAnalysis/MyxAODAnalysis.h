#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TH2D.h>
#include <TTree.h>
#include <TFile.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/Vertex.h>
//#include <TrackVertexAssociationTool.h>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  // Configuration, and any other types of variables go here.
  //VAriables for the conversion/K0 candidates
  long runNumber, eventNumber;
  float x, y, z, r, RxyError;
  float px, py, pz, chi2;
  float dphi;
  float  ee_mass;
  float Kshort_mass, Kshort_massError;
  float Lambda_mass, Lambda_massError;
  float Lambdabar_mass, Lambdabar_massError;
  float minD0, maxD0;
  float minTheta, maxTheta;
  float minPT, maxPT;
  float openingAngle;
  int totalPix, totalSCTHoles;
  bool passResVeto;
  //Variables for the pi0 candidates
  float piPT, piM, piPT1, piPT2, piR1,piR2;
  float piminD01, piminD02;
  int piNPix1, piNPix2;
  float piMaxChi2, piMaxDphi;
  
  //TH1 *m_myHist;
};

#endif
