#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <TLorentzVector.h>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  const double loose_mee = 50;

  ANA_CHECK (book (TH1D ("h_vertex_x", "h_vertex_x", 100, -20, 20))); // vertex pos, mm
  ANA_CHECK (book (TH1D ("h_vertex_y", "h_vertex_y", 100, -20, 20))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_vertex_xy", "h_vertex_xy", 100, -300, 300, 100, -300, 300))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_xy", "h_pos_xy", 100, -300, 300, 100, -300, 300))); // vertex pos, mm
  ANA_CHECK (book (TH1D ("h_vertex_r", "h_vertex_r", 100, 0, 400))); // vertex radius, mm
  ANA_CHECK (book (TH1D ("h_pos_r", "h_pos_r", 100, 0, 400))); // vertex radius, mm
  ANA_CHECK (book (TH1D ("h_pos_ntrk", "h_pos_ntrk", 10, -0.5, 9.5))); // vertex radius, mm
  ANA_CHECK (book (TH1D ("h_pos_q", "h_pos_q", 7, -3.5, 3.5))); // vertex radius, mm
  ANA_CHECK (book (TH1D ("h_vertex_ee_mass", "h_vertex_ee_mass", 140, 0, 700))); // mass, GeV
  ANA_CHECK (book (TH1D ("h_vertex_k0_mass", "h_vertex_k0_mass", 100, 200, 800))); // 
  ANA_CHECK (book (TH1D ("h_k0_k0_mass", "h_k0_k0_mass", 100, 200, 800))); // 
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_faildphi", "h_k0_k0_mass_faildphi", 100, 200, 800))); //
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_nopix", "h_k0_k0_mass_nopix", 100, 200, 800))); //
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_pix", "h_k0_k0_mass_pix", 100, 200, 800))); // 
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_failchi", "h_k0_k0_mass_failchi", 100, 200, 800))); // mass, GeV
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_passchi", "h_k0_k0_mass_passchi", 100, 200, 800))); // mass, GeV
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_passRxyError", "h_k0_k0_mass_passRxyError", 100, 200, 800))); // mass, GeV
  ANA_CHECK (book (TH1D ("h_k0_k0_mass_failRxyError", "h_k0_k0_mass_failRxyError", 100, 200, 800))); // mass, GeV
  ANA_CHECK (book (TH1D ("h_vertex_dphi", "h_vertex_dphi", 100, 0.,3.1416))); // vertex radius, mm

  ANA_CHECK (book (TH2D ("h_vertex_dPhiOpening_v_m", "h_vertex_dPhiOpening_v_m", 100, 0, loose_mee,200, 0, 0.5))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_vertex_Error_v_m", "h_vertex_Error_v_m", 200, 0, 600, 100, 0, loose_mee))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_vertex_m_v_dphi", "h_vertex_m_v_dphi", 100, 0, 0.2, 100, 0, 500))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_r_v_dphi", "h_pos_r_v_dphi", 100, 0, 0.5, 100, 0, 300))); // vertex pos, mm

  ANA_CHECK (book (TH2D ("h_pos_r_v_z", "h_pos_r_v_z", 100, -800, 800, 100, 0, 400))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_r_v_z_passchi", "h_pos_r_v_z_passchi", 100, -800, 800, 100, 0, 400))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_logChi2_v_r", "h_pos_logChi2_v_r", 100, 0, 400, 100, -6, 3))); // vertex pos, mm and chi2
  ANA_CHECK (book (TH2D ("h_pos_RxyError_v_r", "h_pos_RxyError_v_r", 100, 0, 400, 100, 0, 100))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_RxyError_v_r_nopix", "h_pos_RxyError_v_r_nopix", 100, 0, 400, 100, 0, 50))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_RxyError_v_m_nopix", "h_pos_RxyError_v_m_nopix", 100, 0, loose_mee, 100, 0, 50))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_pt_v_r", "h_pos_pt_v_r", 100, 0, 400, 100, 0, 5000))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_minD0_v_r", "h_pos_minD0_v_r", 100, 0, 400, 100, 0., 200))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_maxD0_v_r", "h_pos_maxD0_v_r", 100, 0, 400, 100, 0., 200))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_ptRat_v_r", "h_pos_ptRat_v_r", 100, 0, 400, 100, 0., 1))); // vertex pos, mm

  ANA_CHECK (book (TH2D ("h_pos_totalPix_v_r", "h_pos_totalPix_v_r", 100,0.,400.,10, -0.5, 9.5))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_pos_totalSCTHoles_v_r", "h_pos_totalSCTHoles_v_r", 100,0.,400,10, -0.5, 9.5))); // vertex pos, mm

  ANA_CHECK (book (TH2D ("h_k0_r_v_z", "h_k0_r_v_z", 100, -800, 800, 100, 0, 400))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_k0_logChi2_v_m", "h_k0_logChi2_v_m", 100, 200, 700, 100, -6, 3))); // vertex posm, mm and chi2
  ANA_CHECK (book (TH2D ("h_k0_dphi_v_m", "h_k0_dphi_v_m", 100,200,700,100, 0, 0.2))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_k0_RxyError_v_m", "h_k0_RxyError_v_m", 100,200,700,100, 0, 50))); // dr, mm v m, GeV
  ANA_CHECK (book (TH2D ("h_k0_RxyError_v_m_nopix", "h_k0_RxyError_v_m_nopix", 100,200,700,100, 0, 50))); // dr, mm v m, GeV
  ANA_CHECK (book (TH2D ("h_k0_totalPix_v_m", "h_k0_totalPix_v_m", 100,200,700,10, -0.5, 9.5))); // vertex pos, mm
  ANA_CHECK (book (TH2D ("h_k0_pt_v_r", "h_k0_pt_v_r", 100, 0, 400, 100, 0, 10000))); // pt, mev or gev? , vertex pos, mm
  ANA_CHECK (book (TH2D ("h_k0_RxyError_v_r", "h_k0_RxyError_v_r", 100, 0, 400, 100, 0, 20))); // vertex pos, mm

  ANA_CHECK (book (TH1D ("h_k0_totalPix", "h_k0_totalPix", 10, -0.5, 9.5))); // vertex pos, mm
  ANA_CHECK (book (TH1D ("h_k0_totalSCTHoles", "h_k0_totalSCTHoles", 10, -0.5, 9.5))); // vertex pos, mm

  ANA_CHECK (book (TH1D ("h_x17_m",            "h_x17_m",            200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_resVeto",    "h_x17_m_resVeto",    200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_rdVeto",    "h_x17_m_rdVeto",    200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noDphi", "    h_x17_m_noDphi",     200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noZ",        "h_x17_m_noZ",        200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noPix",      "h_x17_m_noPix",      200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noSCT",      "h_x17_m_noSCT",      200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noR",        "h_x17_m_noR",        200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noChi2",     "h_x17_m_noChi2",     200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noRxyError", "h_x17_m_noRxyError", 200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_m_noMinD0",    "h_x17_m_noMinD0",    200, 0.0, 500))); // x17 candidates mass spectrum
  ANA_CHECK (book (TH1D ("h_x17_mk0_noDphi", "    h_x17_mk0_noDphi",     100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noZ",        "h_x17_mk0_noZ",        100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noPix",      "h_x17_mk0_noPix",      100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noSCT",      "h_x17_mk0_noSCT",      100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noR",        "h_x17_mk0_noR",        100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noChi2",     "h_x17_mk0_noChi2",     100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noRxyError", "h_x17_mk0_noRxyError", 100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH1D ("h_x17_mk0_noMinD0",     "h_x17_mk0_noMinD0",   100, 400, 600))); // x17 k0 mass
  ANA_CHECK (book (TH2D ("h_x17_r_v_m", "h_x17_r_v_m", 200, 0.0, 500,100,50.,350.))); // mass v r
  ANA_CHECK (book (TH2D ("h_x17_z_v_m", "h_x17_z_v_m", 200, 0.0, 500,100,-700,700.))); // mass v z
  ANA_CHECK (book (TH2D ("h_x17_r_v_m_resVeto", "h_x17_r_v_m_resVeto", 100, 0.0, 100,100,50.,350.))); // mass v r
  ANA_CHECK (book (TH2D ("h_x17_r_v_dphi_resVeto", "h_x17_r_v_dphi_resVeto", 20, 0.0, 0.02,50,50.,350.))); // mass v r
  ANA_CHECK (book (TH2D ("h_x17_r_v_logdphi_resVeto", "h_x17_r_v_logdphi_resVeto", 50, -20, -1,50,50.,350.))); // t v lod(dphi);
  ANA_CHECK (book (TH2D ("h_x17_dphi_v_m", "h_x17_dphi_v_m", 50, 0.0, 100,50,0.,0.20))); // dphi v m
  ANA_CHECK (book (TH2D ("h_x17_r_v_m_noDphi", "h_x17_r_v_m_noDphi", 100, 0.0, 100,100,50.,350.))); // mass v r
  ANA_CHECK (book (TH2D ("h_x17_z_v_m_resVeto", "h_x17_z_v_m_resVeto", 100, 0.0, 100,100,-700,700.))); // mass v z
  ANA_CHECK (book (TH2D ("h_x17_z_v_r", "h_x17_z_v_r", 200, 0.0, 300,100,-700,700.))); // mass v z
  ANA_CHECK (book (TH2D ("h_x17_y_v_x", "h_x17_y_v_x", 500, -300.0, 300,500,-300,300.))); // mass v z
  //  ANA_CHECK (book (TH2D ("h_x17_y_v_x", "h_x17_y_v_x", 200, -300.0, 300,200,-300,300.))); // mass v z
  ANA_CHECK (book (TH1D ("h_x17_mk0", "h_x17_mk0", 100, 400.0, 600))); // mass in k0 hypothesis
  ANA_CHECK (book (TH1D ("h_x17_mLambda", "h_x17_mLambda", 200, 1000, 1400))); // mass in Lambda hypothesis
  ANA_CHECK (book (TH1D ("h_x17_pullk0", "h_x17_pullk0", 100, -50.,50.)));
  ANA_CHECK (book (TH1D ("h_x17_pullLambda", "h_x17_pullLambda", 100, -50.,50.)));
  ANA_CHECK (book (TH1D ("h_x17_pullLambdabar", "h_x17_pullLambdabar", 100, -50.,50.)));
  ANA_CHECK (book (TH2D ("h_x17_mk0_v_mee", "h_x17_mk0_v_mee", 100,0.,500,100, 250.0, 800))); // 
  ANA_CHECK (book (TH2D ("h_x17_mk0_v_mlam", "h_x17_mk0_v_mlam", 100,1050.,1400,100, 250.0, 800))); // 
  ANA_CHECK (book (TH2D ("h_x17_mlambda_v_mee", "h_x17_mlambda_v_mee", 100,0.,500,100, 1000., 1800))); // 
  ANA_CHECK (book (TH2D ("h_x17_mlambdabar_v_mee", "h_x17_mlambdabar_v_mee", 100,0.,500,100, 1000., 1800))); // 

  ANA_CHECK (book (TH2D ("h_x17_ptRat_v_m_resVeto", "h_x17_ptRat_v_m_resVeto", 100, 0.0, 100,100,0.,1.))); // ptratio v mass 
  ANA_CHECK (book (TH2D ("h_x17_logChi2_v_m_resVeto", "h_x17_logChi2_v_m_resVeto", 100, 0.0, 100,100,-5,1.))); // 
  ANA_CHECK (book (TH2D ("h_x17_minD0_v_m_resVeto", "h_x17_minD0_v_m_resVeto", 50, 0.0, 100,50,0.,120.))); // 
  ANA_CHECK (book (TH2D ("h_x17_maxD0_v_m_resVeto", "h_x17_maxD0_v_m_resVeto", 50, 0.0, 100,50,0.,120.))); // 
  ANA_CHECK (book (TH2D ("h_x17_pT_v_m_resVeto", "h_x17_pT_v_m_resVeto", 100, 0.0, 100,100,0.,10000.))); // 
  ANA_CHECK (book (TH2D ("h_x17_d_v_m_resVeto", "h_x17_d_v_m_resVeto", 100, 0.0, 50,100,0.,50.))); // 
  ANA_CHECK (book (TH2D ("h_x17_d_v_m_nearSCT", "h_x17_d_v_m_nearSCT", 100, 0.0, 50,100,0.,50.))); // 
  ANA_CHECK (book (TH2D ("h_x17_d_v_r_resVeto", "h_x17_d_v_r_resVeto", 100, 50.0, 350,100,0.,50.))); // 
  ANA_CHECK (book (TH2D ("h_x17_d_v_r_10m20", "h_x17_d_v_r_10m20", 100, 50.0, 350,100,0.,50.))); // 
  ANA_CHECK (book (TH2D ("h_x17_dtheta_v_m_resVeto", "h_x17_dtheta_v_m_resVeto", 100, 0.0, 50,100,0.,0.3))); // 
  ANA_CHECK (book (TH2D ("h_x17_dtheta_v_r_resVeto", "h_x17_dtheta_v_r_resVeto", 100, 50.0, 350,100,0.,0.3))); // 
  ANA_CHECK (book (TH2D ("h_x17_dtheta_v_r_10m20", "h_x17_dtheta_v_r_10m20", 100, 50.0, 350,100,0.,0.3))); // 
  ANA_CHECK (book (TH2D ("h_x17_dPhiOpening_v_m", "h_x17_dPhiOpening_v_m", 100, 0, loose_mee,200, 0, 0.5))); // vertex pos, mm

  ANA_CHECK  (book (TH1D ("h_gamma_pt", "h_gamma_pt", 100, 0.0, 5000))); // pt of conversions
  //
  ANA_CHECK  (book (TH1D ("h_pi0_all", "h_pi0_all", 100, 0.0, 500))); // mass of pairs of cneversions
  ANA_CHECK  (book (TH1D ("h_pi0_passPtRat", "h_pi0_passPtRat", 100, 0.0, 500))); // mass of pairs of cneversions
  ANA_CHECK  (book (TH1D ("h_pi0_rMinMax", "h_pi0_rMinMax", 100, 0.0, 500))); // mass of pairs of cneversions
  ANA_CHECK  (book (TH1D ("h_pi0_rMin", "h_pi0_rMin", 100, 0.0, 500))); // mass of pairs of coneversions
  ANA_CHECK  (book (TH2D ("h_pi0_ptrat_v_m", "h_pi0_ptrat_v_m", 100, 0.0, 500,50,0.,1.))); // ptrat v mass of pairs of conversions
  ANA_CHECK  (book (TH2D ("h_pi0_r_v_m", "h_pi0_r_v_m", 100, 0.0, 500,50,0.,300.))); // r v mass of pairs of conversions

  ANA_CHECK  (book (TTree ("v0","Conversion Candidate ntuples")));

  TTree * myTree = tree ("v0");
  myTree->Branch("runNumber",&runNumber);
  myTree->Branch("eventNumber",&eventNumber);
  myTree->Branch("x",&x);
  myTree->Branch("y",&y);
  myTree->Branch("z",&z);
  myTree->Branch("r",&r);
  myTree->Branch("RxyError",&RxyError);
  myTree->Branch("px",&px);
  myTree->Branch("py",&py);
  myTree->Branch("pz",&pz);
  myTree->Branch("chi2",&chi2);
  myTree->Branch("dphi",&dphi);
  myTree->Branch("ee_mass",&ee_mass);
  myTree->Branch("Kshort_mass",&Kshort_mass);
  myTree->Branch("Kshort_massError",&Kshort_massError);
  myTree->Branch("Lambda_mass",&Lambda_mass);
  myTree->Branch("Lambda_massError",&Lambda_massError);
  myTree->Branch("Lambdabar_mass",&Lambdabar_mass);
  myTree->Branch("Lambdabar_massError",&Lambdabar_massError);
  myTree->Branch("minD0",&minD0);
  myTree->Branch("maxD0",&maxD0);
  myTree->Branch("minTheta",&minTheta);
  myTree->Branch("maxTheta",&maxTheta);
  myTree->Branch("minPT",&minPT);
  myTree->Branch("maxPT",&maxPT);
  myTree->Branch("openingAngle",&openingAngle);
  myTree->Branch("totalPix",&totalPix);
  myTree->Branch("totalSCTHoles",&totalSCTHoles);
  myTree->Branch("passResVeto",&passResVeto);

  ANA_CHECK  (book (TTree ("pi0","pi0 Candidate ntuples")));

  TTree * myPiTree = tree ("pi0");
  myPiTree->Branch("runNumber",&runNumber);
  myPiTree->Branch("eventNumber",&eventNumber);
  myPiTree->Branch("pT",&piPT);
  myPiTree->Branch("M",&piM);
  myPiTree->Branch("maxChi2",&piMaxChi2);
  myPiTree->Branch("maxDphi",&piMaxDphi);
  myPiTree->Branch("PT1",&piPT1);
  myPiTree->Branch("PT2",&piPT2);
  myPiTree->Branch("R1",&piR1);
  myPiTree->Branch("R2",&piR2);
  myPiTree->Branch("NPix1",&piNPix1);
  myPiTree->Branch("NPix2",&piNPix2);
  myPiTree->Branch("minD01",&piminD01);
  myPiTree->Branch("minD02",&piminD02);

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  typedef std::vector< ElementLink< xAOD::TrackParticleContainer > > TrackParticleLinks_t;
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  const double  maxConvMass = 50;
  const double zCut = 400;
  const double dPhiCut = 0.005;
  const double chi2Cut = 2.;
  const double minD0Cut = 0.; // was 10, biased.
  const double RxyErrorCut = 150000.;
  const double loose_mee = 50;
  const double xc = 1.0;
  const double yc = 0.5;
  const double me = 0.511;
  const double bField = 2.0;
  const double rMin = 150.;
  const double rMax = 200.;

  std::vector<TLorentzVector> conversion_list;
  std::vector<double> conversion_r;
  std::vector<int> conversion_npix;
  std::vector<float> conversion_minD0;
  std::vector<float> conversion_chi2;
  std::vector<float> conversion_dphi;
  
  //  ANA_MSG_INFO ("in execute");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  //  ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  /*
  // loop over the jets in the container
  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMTopoJets"));
  for (const xAOD::Jet* jet : *jets) {
    ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" jet pt="<<jet->pt());
    hist ("h_jetPt")->Fill (jet->pt() * 0.001); // GeV
  } // end for loop over jets
  */

  // Get the primary vertex.
  const xAOD::VertexContainer* primaries = nullptr;
  Amg::Vector3D primaryPos(0.,0.,0.);
  double ptmax2 = 0;
  ANA_CHECK (evtStore()->retrieve (primaries, "PrimaryVertices"));
  for (const xAOD::Vertex* primary : *primaries) {
    //    double sumPt2 = primary->auxdata<float>("sumPt2");
    double sumPt2 = 1.;
    if (sumPt2 > ptmax2) {
      primaryPos = primary->position();
      ptmax2 = sumPt2;
    }
  }
  // Make a list of all the tracks used in all the vertices  
  //  TrackParticleLinks_t trackLinksPlus;
  //  TrackParticleLinks_t trackLinksMinus;
  TrackParticleLinks_t conversion_trackp;
  TrackParticleLinks_t conversion_trackm;

    // loop over the vertices in the container
  const xAOD::VertexContainer* vertices = nullptr;
  ANA_CHECK (evtStore()->retrieve (vertices, "V0UnconstrVertices"));
  for (const xAOD::Vertex* vertex : *vertices) {
    x = vertex->x();
    y = vertex->y();
    z = vertex->z();
    bool passZ = fabs(z)<zCut;
    r = sqrt((x-xc)*(x-xc)+(y-yc)*(y-yc));
    bool passR = (r>rMin && r< rMax);
    hist ("h_vertex_x")->Fill (x); // mm
    hist ("h_vertex_y")->Fill (y); // mm
    hist ("h_vertex_xy")->Fill (x,y); // mm
    hist ("h_vertex_r")->Fill (r); // mm
    ee_mass = vertex->auxdata<float>("ee_mass");
    Kshort_mass = vertex->auxdata<float>("Kshort_mass");
    Kshort_massError = vertex->auxdata<float>("Kshort_massError");
    Lambda_mass = vertex->auxdata<float>("Lambda_mass");
    Lambda_massError = vertex->auxdata<float>("Lambda_massError");
    Lambdabar_mass = vertex->auxdata<float>("Lambdabar_mass");
    Lambdabar_massError = vertex->auxdata<float>("Lambdabar_massError");
    double pullk0 = (Kshort_mass-497.6)/Kshort_massError;
    double pullLambda = (Lambda_mass-1117.3)/Lambda_massError;
    double pullLambdabar = (Lambdabar_mass-1117.3)/Lambdabar_massError;
    passResVeto = abs(pullk0) > 3 && abs(pullLambda) > 3 && abs (pullLambdabar) > 3;
    RxyError = vertex->auxdata<float>("RxyError");
    bool passRxyError = RxyError < RxyErrorCut; 
    px = vertex->auxdata<float>("px");
    py = vertex->auxdata<float>("py");
    pz = vertex->auxdata<float>("pz");
    double pt = sqrt(px*px+py*py);
    //    ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" vertex r = "<<r<<" Rxy = "<<Rxy);
    hist ("h_vertex_ee_mass")->Fill (ee_mass); // mm
    hist ("h_vertex_k0_mass")->Fill (Kshort_mass); // mm
    chi2 = vertex->chiSquared();
    bool passChi2 = chi2 < chi2Cut;
    double logChi2 = -4;
    if (chi2 > 0) logChi2 = log(chi2);
    Amg::Vector3D relativePos = vertex->position() -  primaryPos;
    Amg::Vector3D momentum(vertex->auxdata<float>("px"),vertex->auxdata<float>("py"),vertex->auxdata<float>("pz"));
    dphi = fabs(relativePos.deltaPhi(momentum));
    bool passDphi = dphi < dPhiCut;
//    ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" vertex r = "<<r<<" Rxy = "<<Rxy<<" relpos x="<<relativePos.x()," y="<<relativePos.x()<<std::endl; );

    if (passR) {
      hist ("h_vertex_dphi")->Fill (dphi); // 
      hist ("h_vertex_m_v_dphi")->Fill (dphi,ee_mass); // 
    }
    runNumber = eventInfo->runNumber();
    eventNumber = eventInfo->eventNumber();

    const TrackParticleLinks_t& trackLinks = vertex->trackParticleLinks();
    minD0 = 1000;
    maxD0 = 0;
    minTheta = 1000;
    maxTheta = -1000;
    int totalq = 0;
    int ntrk = 0;
    totalPix  = 0;
    totalSCTHoles  = 0;
    minPT = 1E7;
    maxPT = 0;
    double pT[2],theta[2], rad[2], p[2], E[2];
    ElementLink< xAOD::TrackParticleContainer > trackLinkPlus;
    ElementLink< xAOD::TrackParticleContainer > trackLinkMinus;

    
    for (ElementLink< xAOD::TrackParticleContainer > aTrackLink : trackLinks) {
      //      if (ee_mass < 100) ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" Vertex at x="<<x<<" has track at "<<(*aTrackLink));
      int charge = (*aTrackLink)->charge();
      int iq = 0;
      if (charge<1) iq=1;
      pT[iq] = (*aTrackLink)->pt();
      if (pT[iq] > maxPT) maxPT = pT[iq];
      if (pT[iq] < minPT) minPT = pT[iq];
      double d0 = fabs((*aTrackLink)->d0());
      theta[iq] = fabs((*aTrackLink)->theta());
      p[iq] = fabs(pT[iq]/sin(theta[iq]));
      E[iq] = sqrt(me*me + p[iq]*p[iq]);
      rad[iq] = pT[iq]/(0.3*bField);
      if (d0 < minD0) minD0 = d0;
      if (d0 > maxD0) maxD0 = d0;
      if (theta[iq] < minTheta) minTheta = theta[iq];
      if (theta[iq] > maxTheta) maxTheta = theta[iq];
      totalq += charge;
      //      if (charge > 0) trackLinksPlus.push_back(aTrackLink);
      //      else trackLinksMinus.push_back(aTrackLink);
      if (charge > 0) trackLinkPlus = aTrackLink;
      else trackLinkMinus = aTrackLink;
      ntrk++;
      unsigned char npix = (*aTrackLink)->auxdata<unsigned char>("numberOfPixelHits");
      unsigned char nSCTHoles = (*aTrackLink)->auxdata<unsigned char>("numberOfSCTHoles");
      totalPix+=npix;
      totalSCTHoles+=nSCTHoles;
    }
    double ptRat = minPT/maxPT;

    bool passPix = (totalPix==0);
    bool passSCT = (totalSCTHoles==0);
    bool passMinD0 = (minD0 > minD0Cut);
    double cosOpeningAngle = (2*me*me + 2*E[0]*E[1] - ee_mass*ee_mass)/(2*p[0]*p[1]);
     openingAngle = 0;
    if (cosOpeningAngle > 1 ) {
      ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" vertex mass "<<ee_mass<<" has cosOpeningAngle="<<cosOpeningAngle<<" p[0]="<<p[0]<<" p[1]="<<p[1]<<" pT[0]="<<pT[0]<<" pT[1]="<<pT[1]<<" theta[0]="<<theta[0]<<" theta[1]="<<theta[1]);
      openingAngle =  3.14159265358979;
    } else  {
      if (cosOpeningAngle < -1) {
	//     ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" vertex mass "<<ee_mass<<" has cosOpeningAngle="<<cosOpeningAngle<<" p[0]="<<p[0]<<" p[1]="<<p[1]<<" pT[0]="<<pT[0]<<" pT[1]="<<pT[1]<<" theta[0]="<<theta[0]<<" theta[1]="<<theta[1]);
	openingAngle =  0;
      } else {
	openingAngle =  acos(cosOpeningAngle);
      }
    }
   
    double dtheta = fabs(theta[0]-theta[1]);
    double dPhiOpening = 0;
    if (openingAngle > dtheta) dPhiOpening = sqrt(openingAngle*openingAngle - dtheta*dtheta);
    // Now theta = 2 asin( d/2r1) + 2 asin( d/2r2)
    // Approximately  theta =   d/r1 +  d/r2
   // So d ~ r1 r2 theta / (r1+r2)
   double d = rad[0]*rad[1]*dPhiOpening/(rad[0]+rad[1]);
   // Back-calculate opening angle from d:
   double dPhiTest = 2*(asin(d/rad[0]/2.)+asin(d/rad[1]/2.));
   for (int i=0;i<3;++i) {
     // iterate the d for the difference
     d = d*dPhiOpening/dPhiTest;
     // Back-calculate opening angle from d:
     dPhiTest = 2*(asin(d/rad[0]/2.)+asin(d/rad[1]/2.));
   }
   if (dPhiTest/dPhiOpening > 1.1 || dPhiTest/dPhiOpening < 0.9) {
     ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" vertex mass "<<ee_mass<<" has openingAngle="<<openingAngle<<", dtheta="<<dtheta<<", dPhiOpening="<<dPhiOpening<<" and d="<<d<<" dPhiTest="<<dPhiTest);
     d = d*dPhiOpening/dPhiTest;
     // Back-calculate opening angle from d:
     dPhiTest = 2*(asin(d/rad[0]/2.)+asin(d/rad[1]/2.));
     ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" vertex mass "<<ee_mass<<" has openingAngle="<<openingAngle<<", dtheta="<<dtheta<<", dPhiOpening="<<dPhiOpening<<" and d="<<d<<" dPhiTest="<<dPhiTest);     
   }
    
   hist ("h_vertex_dPhiOpening_v_m")->Fill(ee_mass,dPhiOpening);
   hist ("h_vertex_Error_v_m")->Fill(ee_mass,RxyError);

   // Fill the ntuple with  candidates passing dphi and radius cuts to reduce data volume a little.
   if (dphi < 0.5 &&  r > 50)  {
     tree("v0")->Fill();
   }
   
   // Plot some low mass vertex characteristics
   if (ee_mass < loose_mee && dphi < 0.5) {
     hist ("h_pos_RxyError_v_r")->Fill(r,RxyError);
     if (passRxyError) {
       hist ("h_pos_r_v_z")->Fill(z,r);
       if ( passZ ) {	
	 hist ("h_pos_r_v_dphi")->Fill (dphi,r); //
	 if (passDphi) {
	   hist ("h_pos_totalPix_v_r")->Fill (r,totalPix); // 
	   if (passPix) {
	     hist ("h_pos_totalSCTHoles_v_r")->Fill (r,totalSCTHoles); //
	     hist ("h_pos_RxyError_v_m_nopix")->Fill(ee_mass,RxyError);
	     hist ("h_pos_RxyError_v_r_nopix")->Fill(r,RxyError);
	     hist ("h_pos_xy")->Fill (x,y); // mm
	     hist ("h_pos_r")->Fill (r); // mm
	     hist ("h_pos_logChi2_v_r")->Fill(r,logChi2);
	     if (passChi2) {
	       hist ("h_pos_r_v_z_passchi")->Fill(z,r);
	       hist ("h_pos_ntrk")->Fill(ntrk);
	       hist ("h_pos_q")->Fill(totalq);
	       hist ("h_pos_pt_v_r")->Fill (r,pt); // 
	       hist ("h_pos_minD0_v_r")->Fill (r,minD0); //
	       hist ("h_pos_maxD0_v_r")->Fill (r,maxD0); //
	       hist ("h_pos_ptRat_v_r")->Fill (r,ptRat); //
	     }
	   }
	 }
	}
      }
    }
    // Plot the mass distribution of the candidate x17s
    if (passZ &&  passPix && passSCT && passR && passChi2 && passRxyError && passMinD0) {
      // All cuts bar the dPhi
      hist ("h_x17_mk0_noDphi")->Fill(Kshort_mass);
      if (passResVeto) {
	hist ("h_x17_r_v_m_noDphi")->Fill(ee_mass,r);
	hist ("h_x17_dphi_v_m")->Fill(ee_mass,dphi);
	hist ("h_x17_m_noDphi")->Fill(ee_mass);
      }
    }
    if (passDphi && passPix && passSCT && passR && passChi2 && passRxyError  && passMinD0) {
      hist ("h_x17_mk0_noZ")->Fill(Kshort_mass);
      hist ("h_x17_z_v_m")->Fill(ee_mass,z);
      if (passResVeto) {
        hist ("h_x17_m_noZ")->Fill(ee_mass);
        hist ("h_x17_z_v_m_resVeto")->Fill(ee_mass,z);
      }
  }
    if (passDphi && passZ && passSCT && passR && passChi2 && passRxyError  && passMinD0) {
    hist ("h_x17_mk0_noPix")->Fill(Kshort_mass);
    if (passResVeto) {
      hist ("h_x17_m_noPix")->Fill(ee_mass);
    }
    
    }
    if (passDphi && passZ && passPix && passR && passChi2 && passRxyError  && passMinD0) {
      hist ("h_x17_mk0_noSCT")->Fill(Kshort_mass);
      if (passResVeto) hist ("h_x17_m_noSCT")->Fill(ee_mass);
    }
    if (passDphi && passZ &&  passPix && passSCT && passChi2 && passRxyError  && passMinD0) {
      // All cuts bar the r
      hist ("h_x17_mk0_noR")->Fill(Kshort_mass);
      hist ("h_x17_r_v_m")->Fill(ee_mass,r);
      if (passResVeto) {
       hist ("h_x17_dPhiOpening_v_m")->Fill(ee_mass,dPhiOpening);
	hist ("h_x17_r_v_m_resVeto")->Fill(ee_mass,r);
	hist ("h_x17_r_v_dphi_resVeto")->Fill(dphi,r);
	double logdphi = -10;
	if (dphi > 0) logdphi = log(dphi);
	hist ("h_x17_r_v_logdphi_resVeto")->Fill(logdphi,r);
	hist ("h_x17_m_noR")->Fill(ee_mass);
        if (ee_mass < 10) hist ("h_x17_y_v_x")->Fill(x,y);
      }
    }

    if (passDphi && passZ &&  passPix && passSCT && passR && passRxyError  && passMinD0) {
      hist ("h_x17_mk0_noChi2")->Fill(Kshort_mass);
      if (passResVeto) hist ("h_x17_m_noChi2")->Fill(ee_mass);
    }
    if (passDphi && passZ &&  passPix && passSCT && passR && passChi2  && passMinD0) {
      hist ("h_x17_mk0_noRxyError")->Fill(Kshort_mass);
      if (passResVeto) hist ("h_x17_m_noRxyError")->Fill(ee_mass);
    }
    if (passDphi &&  passPix && passSCT &&  passChi2 && passRxyError  && passMinD0) {
      // no passR
      if (passResVeto) {
	if (ee_mass < loose_mee) {
	  hist ("h_x17_z_v_r")->Fill(r,z);
	}
	hist ("h_x17_d_v_m_resVeto")->Fill(ee_mass,d); //d in mm, m in MeV
	hist ("h_x17_dtheta_v_m_resVeto")->Fill(ee_mass,dtheta); //d in mm, m in MeV
	if (r<310 && r> 260) hist ("h_x17_d_v_m_nearSCT")->Fill(ee_mass,d); //d in mm, m in MeV
	hist ("h_x17_d_v_r_resVeto")->Fill(r,d); //d in mm, r in mm
	hist ("h_x17_dtheta_v_r_resVeto")->Fill(r,dtheta); //d in mm, r in mm
	if (ee_mass > 10 && ee_mass < 20) hist ("h_x17_d_v_r_10m20")->Fill(r,d); //d in mm, r in mm
	if (ee_mass > 10 && ee_mass < 20) hist ("h_x17_dtheta_v_r_10m20")->Fill(r,dtheta); //d in mm, r in mm
      }
    }

    if (passDphi && passZ &&  passPix && passSCT && passR && passChi2 && passRxyError) {
      hist ("h_x17_mk0_noMinD0")->Fill(Kshort_mass);
      if (passResVeto) hist ("h_x17_m_noMinD0")->Fill(ee_mass);
    }
    
    if (passDphi && passZ &&  passPix && passSCT && passR && passChi2 && passRxyError && passMinD0) {
      hist ("h_x17_mk0")->Fill(Kshort_mass);
      hist ("h_x17_mLambda")->Fill(Lambda_mass);
      hist ("h_x17_pullk0")->Fill(pullk0);
      hist ("h_x17_pullLambda")->Fill(pullLambda);
      hist ("h_x17_pullLambdabar")->Fill(pullLambdabar);
      hist ("h_x17_mk0_v_mee")->Fill(ee_mass,Kshort_mass);
      hist ("h_x17_mk0_v_mlam")->Fill(Lambda_mass,Kshort_mass);
      hist ("h_x17_mlambda_v_mee")->Fill(ee_mass,Lambda_mass);
      hist ("h_x17_mlambdabar_v_mee")->Fill(ee_mass,Lambdabar_mass);
      hist ("h_x17_m")->Fill(ee_mass);
      if (passResVeto) {
	hist ("h_x17_m_resVeto")->Fill(ee_mass);
	if ((r-d) > rMin && (r+d) < rMax) hist ("h_x17_m_rdVeto")->Fill(ee_mass);
	hist ("h_x17_ptRat_v_m_resVeto")->Fill(ee_mass,ptRat);
	hist ("h_x17_logChi2_v_m_resVeto")->Fill(ee_mass,logChi2);
	hist ("h_x17_minD0_v_m_resVeto")->Fill(ee_mass,minD0);
	hist ("h_x17_maxD0_v_m_resVeto")->Fill(ee_mass,maxD0);
	hist ("h_x17_pT_v_m_resVeto")->Fill(ee_mass,pt); //pt is vertex, pT[2] the tracks
      }

    }
    // Plot some k0 candidate characteristics in the interesting region
    if (passR && passZ && dphi<0.5) {
      hist ("h_k0_dphi_v_m")->Fill(Kshort_mass,dphi);
      if (passDphi) {
	hist ("h_k0_k0_mass")->Fill (Kshort_mass); // mm
	hist ("h_k0_RxyError_v_m")->Fill(Kshort_mass,RxyError);
	hist ("h_k0_totalPix_v_m")->Fill(Kshort_mass,totalPix);
	if (passPix && passSCT) {
	  hist ("h_k0_k0_mass_nopix")->Fill(Kshort_mass);
  	  hist ("h_k0_RxyError_v_m_nopix")->Fill(Kshort_mass,RxyError);
  	  hist ("h_k0_logChi2_v_m")->Fill(Kshort_mass,logChi2);
	  if (passChi2) {
	    hist ("h_k0_k0_mass_passchi")->Fill(Kshort_mass);
	    if (passRxyError) {
	      hist ("h_k0_k0_mass_passRxyError")->Fill(Kshort_mass);
	      if (Kshort_mass > 480 && Kshort_mass < 520) {
		hist ("h_k0_r_v_z")->Fill(z,r);
		hist ("h_k0_pt_v_r")->Fill (r,pt); // 
		hist ("h_k0_RxyError_v_r")->Fill (r,RxyError); // 
		hist ("h_k0_totalPix")->Fill (totalPix); // 
		hist ("h_k0_totalSCTHoles")->Fill (totalSCTHoles); //
	      }

	    } else {
	      hist ("h_k0_k0_mass_failRxyError")->Fill(Kshort_mass);
	    }

	  } else {
   	    hist ("h_k0_k0_mass_failchi")->Fill(Kshort_mass);
	  }
	} else {
	  hist ("h_k0_k0_mass_pix")->Fill(Kshort_mass);
	}
      } else {
	hist ("h_k0_k0_mass_faildphi")->Fill (Kshort_mass); // mm
      }
    
    } // end for study of k0 canddates
    // Now build a list of conversions for use in pi0 finding
    //    if (passDphi && passSCT && passChi2 && passRxyError && passMinD0) {
    if ( passSCT && passRxyError) {
      if (ee_mass < maxConvMass) {
	TLorentzVector conversion(px,py,pz,sqrt(px*px+py*py+pz*pz+ee_mass*ee_mass));
	conversion_list.push_back(conversion);
	conversion_r.push_back(r);
	conversion_npix.push_back(totalPix);
	conversion_minD0.push_back(minD0);
	conversion_chi2.push_back(chi2);
	conversion_dphi.push_back(dphi);
	conversion_trackp.push_back(trackLinkPlus);
	conversion_trackm.push_back(trackLinkMinus);
      }
    }
  }
  // Now loop  through the photons looking for pions.

  for (uint i=0;i<conversion_list.size();++i) {
    double pti = conversion_list[i].Pt();
    hist ("h_gamma_pt")->Fill(pti);
    for (uint j=i+1;j<conversion_list.size();++j) {
      if (conversion_trackp[i] == conversion_trackp[j]) continue; // share the positron
      if (conversion_trackm[i] == conversion_trackm[j]) continue; // share the electron

      double ptj = conversion_list[j].Pt();
      TLorentzVector pion = conversion_list[i]+conversion_list[j];
      // File the pi0 ntuple.
      if (pion.M() < 500) {
        piPT = pion.Pt();
        piM = pion.M();
        piPT1 = pti;
        piPT2 = ptj;
        piR1 = conversion_r[i];
        piR2 = conversion_r[j];
        piNPix1 = conversion_npix[i];
        piNPix2 = conversion_npix[j];
        piminD01 = conversion_minD0[i];
        piminD02 = conversion_minD0[j];
	piMaxChi2 = fmax(conversion_chi2[i],conversion_chi2[j]);
	piMaxDphi = fmax(conversion_dphi[i],conversion_dphi[j]);
        tree("pi0")->Fill();
      }
      
      hist ("h_pi0_all")->Fill(pion.M());
      
      double ptRat = pti/ptj;
      if (ptj<pti) ptRat = ptj/pti;
      hist ("h_pi0_ptrat_v_m")->Fill(pion.M(),ptRat);
      if (ptRat < 0.95) {
         hist ("h_pi0_passPtRat")->Fill(pion.M());
         hist ("h_pi0_r_v_m")->Fill(pion.M(),conversion_r[i]);
         hist ("h_pi0_r_v_m")->Fill(pion.M(),conversion_r[j]);
	 if ((conversion_r[i] > rMin && conversion_r[i] < rMax) ||
	     (conversion_r[j] > rMin && conversion_r[j] < rMax)) {
             hist ("h_pi0_rMinMax")->Fill(pion.M());
	 }
	 if (conversion_r[i] > rMin || conversion_r[j] > rMin) {
             hist ("h_pi0_rMin")->Fill(pion.M());
	 }
      }
    }
  }
    
  
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  //  m_myTree->Write();
  //  if (m_hfile) delete m_hfile;
  
  return StatusCode::SUCCESS;
}
