#!/bin/sh

cd `dirname $0`

mkdir -p build
cd build

setupATLAS
asetup AnalysisBase,21.2.159

cmake ../source/
source x86_64-*/setup.sh
make

cd ../run

ATestRun_eljob.py --submission-dir=submitDir
