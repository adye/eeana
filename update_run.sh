#!/bin/sh

cd `dirname $0`

scp -p lxplus.cern.ch:/eos/user/m/murray/ATLAS/eeana/source/MyAnalysis/Root/MyxAODAnalysis.cxx source/MyAnalysis/Root/MyxAODAnalysis.cxx

scp -p lxplus.cern.ch:/eos/user/m/murray/ATLAS/eeana/source/MyAnalysis/MyAnalysis/MyxAODAnalysis.h source/MyAnalysis/MyAnalysis/MyxAODAnalysis.h

cd build
asetup --restore

cd ../build/
cmake ../source/
source x86_64-*/setup.sh
if make ; then
    echo make returned true
else
    echo make returned an error; stop here
    cd ..
    exit 1
fi


cd ../run

ATestRun_eljob.py --submission-dir=submitDir
cd ..
